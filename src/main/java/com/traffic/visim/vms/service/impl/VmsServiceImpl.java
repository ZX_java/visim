package com.traffic.visim.vms.service.impl;

import com.alibaba.fastjson.JSON;
import com.traffic.visim.vms.entity.*;
import com.traffic.visim.vms.jna.VmsJnaDll;
import com.traffic.visim.vms.service.IVmsService;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;

/**
 * @ClassName: VmsServiceImpl
 * @Description: TODO 类注释/描述
 * @module: jeecg-boot-parent
 * @Author: zhouzheng
 * @date: 2023/11/28
 * Copyright© Shandong New Beiyang Information Technology Co., Ltd. . all rights reserved.
 */
@Service
@Slf4j
public class VmsServiceImpl implements IVmsService {
    @Override
    public int send(VmsSendReq vmsSendReq, VmsAreaReq vmsAreaReq, VmsProgramReq vmsProgramReq, VmsScreenReq vmsScreenReq, VmsTextReq vmsTextReq) {
        // 1. Create a screen
       int result = VmsJnaDll.instance.Hd_CreateScreen(vmsScreenReq.getNWidthScreen(),vmsScreenReq.getNHeightScreen(),vmsScreenReq.getNColor(),vmsScreenReq.getNGray(), vmsScreenReq.getNCardType(), 0,0);
       if(result!=0){
           log.info("创建屏幕失败{}",JSON.toJSONString(vmsScreenReq));
           log.info("创建屏幕失败详细信息{}",VmsJnaDll.instance.Hd_GetSDKLastError());
            return result;
       }
        log.info("创建屏幕成功");
        // 2. Add program to screen
        int nProgramID = VmsJnaDll.instance.Hd_AddProgram(vmsProgramReq.getPBoderImgPathProgram(), vmsProgramReq.getNBorderEffectProgram(), vmsProgramReq.getNBorderSpeedProgram(), 0, 0);
        if(nProgramID ==-1){
            log.info("创建节目失败{}", JSON.toJSONString(vmsProgramReq));
              return nProgramID;
        }
        log.info("创建节目成功{}",nProgramID);
        // 3. Add Area to program
        int nAreaID = VmsJnaDll.instance.Hd_AddArea(nProgramID, vmsAreaReq.getNX(), vmsAreaReq.getNY(), vmsAreaReq.getNWidthArea(), vmsAreaReq.getNHeightArea(), vmsAreaReq.getPBoderImgPath(), vmsAreaReq.getNBorderEffect(), vmsAreaReq.getNBorderSpeed(), 0, 0);
        if(nAreaID ==-1){
            log.info("创建区域失败{}",JSON.toJSONString(vmsAreaReq));
               return nAreaID;
        }
        log.info("创建区域成功{}",nAreaID);
        // 4.Add text AreaItem to Area
        int nFontColor = VmsJnaDll.instance.Hd_GetColor(vmsTextReq.getNTextColorR(),vmsTextReq.getNTextColorG(),vmsTextReq.getNTextColorB());
        int nAreaItemID = VmsJnaDll.instance.Hd_AddSimpleTextAreaItem(nAreaID, vmsTextReq.getPText(), nFontColor, vmsTextReq.getNBackGroupColor(), vmsTextReq.getNStyle(), vmsTextReq.getPFontName(), vmsTextReq.getNFontHeight(), vmsTextReq.getNShowEffect(), vmsTextReq.getNShowSpeed(), vmsTextReq.getNClearType(), vmsTextReq.getNStayTime(), 0, 0);
        if(nAreaItemID ==-1){
            log.info("创建文字失败{}",JSON.toJSONString(vmsTextReq));
             return nAreaItemID;
        }
        log.info("创建文字成功{}",nAreaItemID);
        int sendResult = VmsJnaDll.instance.Hd_SendScreen(vmsSendReq.getNSendType(), vmsSendReq.getPStrParams(), vmsSendReq.getPDeviceGUID(), 0, 0);
        if(sendResult ==-1){
            log.info("vms信息发送失败{}",JSON.toJSONString(vmsSendReq));
            log.info("vms信息发送失败详细信息{}",VmsJnaDll.instance.Hd_GetSDKLastError());
        }
        log.info("vms信息发送成功");
        return sendResult;
    }

    @Override
    public int clear(VmsClearScreenReq vmsClearScreenReq) {
        int result = VmsJnaDll.instance.Cmd_ClearScreen(vmsClearScreenReq.getNSendType(),vmsClearScreenReq.getPStrParams(),vmsClearScreenReq.getPDeviceGUID());
        if(result ==-1){
            log.info("vms屏幕清除命令发送失败{}",JSON.toJSONString(vmsClearScreenReq));
            log.info("vms屏幕清除命令发送失败详细信息{}",VmsJnaDll.instance.Hd_GetSDKLastError());
            return result;
        }
        log.info("vms屏幕清除命令发送成功");
        return result;
    }

    @Override
    public int ping(VmsCardOnlineReq vmsCardOnlineReq) {
        int result = VmsJnaDll.instance.Cmd_IsCardOnline(vmsCardOnlineReq.getNSendType(),vmsCardOnlineReq.getPStrParams(),vmsCardOnlineReq.getPDeviceGUID());
        if(result ==-1){
            log.info("vms心跳命令返回失败{}",JSON.toJSONString(vmsCardOnlineReq));
            log.info("vms心跳命令返回失败详细信息{}",VmsJnaDll.instance.Hd_GetSDKLastError());
            return result;
        }
        log.info("vms心跳命令返回成功");
        return result;
    }

    @Override
    public int luminance(VmsSetLuminanceReq vmsSetLuminanceReq) {
        int result = VmsJnaDll.instance.Cmd_SetLuminance(vmsSetLuminanceReq.getNSendType(),vmsSetLuminanceReq.getPStrParams(),vmsSetLuminanceReq.getNLuminance(),vmsSetLuminanceReq.getPDeviceGUID());
        if(result ==-1){
            log.info("vms屏幕亮度调整命令发送失败{}",JSON.toJSONString(vmsSetLuminanceReq));
            log.info("vms屏幕亮度调整命令发送失败详细信息{}",VmsJnaDll.instance.Hd_GetSDKLastError());
            return result;
        }
        log.info("vms屏幕亮度调整命令发送成功");
        return result;
    }


}
