package com.traffic.visim.vms.service;


import com.traffic.visim.vms.entity.*;

/**
 * @ClassName: IVmsService
 * @Description: TODO 类注释/描述
 * @module: jeecg-boot-parent
 * @Author: zhouzheng
 * @date: 2023/11/28
 * Copyright© Shandong New Beiyang Information Technology Co., Ltd. . all rights reserved.
 */
public interface IVmsService {

    int send(VmsSendReq vmsSendReq, VmsAreaReq vmsAreaReq, VmsProgramReq vmsProgramReq, VmsScreenReq vmsScreenReq, VmsTextReq vmsTextReq);

    int clear(VmsClearScreenReq vmsClearScreenReq);

    int ping(VmsCardOnlineReq vmsCardOnlineReq);

    int luminance(VmsSetLuminanceReq vmsSetLuminanceReq);


}
