package com.traffic.visim.vms.jna;

import com.sun.jna.Library;
import com.sun.jna.Native;

/**
 * @ClassName: VmsDll
 * @Description: TODO 类注释/描述
 * @module: jeecg-boot-parent
 * @Author: zhouzheng
 * @date: 2023/11/28
 * Copyright© Shandong New Beiyang Information Technology Co., Ltd. . all rights reserved.
 */
public interface VmsJnaDll extends Library {
    //加载vms动态库
    VmsJnaDll instance = Native.load("HDSDK.dll", VmsJnaDll.class);

    int Hd_CreateScreen(int nWidth, int nHeight, int nColor, int nGray, int nCardType,int pExParamsBuf, int nBufSize);

    int Hd_AddProgram( int pBoderImgPath, int nBorderEffect, int nBorderSpeed, int pExParamsBuf, int nBufSize);

    int Hd_AddArea(int nProgramID, int nX, int nY, int nWidth, int nHeight, int pBoderImgPath, int nBorderEffect, int nBorderSpeed, int pExParamsBuf, int nBufSize);

    int Hd_AddSimpleTextAreaItem(int nAreaID, String pText, int nTextColor, int nBackGroupColor, int nStyle, String pFontName, int nFontHeight,  int nShowEffect, int nShowSpeed,int nClearType, int nStayTime, int pExParamsBuf, int nBufSize);

    int Hd_GetColor(int r, int g, int b);

    int Hd_SendScreen(int nSendType, String pStrParams, int pDeviceGUID, int pExParamsBuf, int nBufSize);

    int Cmd_ClearScreen(int nSendType, String pStrParams, int pDeviceGUID);

    int Cmd_SetLuminance(int nSendType, String pStrParams, int nLuminance, int pDeviceGUID);

    int Cmd_IsCardOnline(int nSendType, String pStrParams, int pDeviceGUID);

    int Hd_GetSDKLastError();
}
