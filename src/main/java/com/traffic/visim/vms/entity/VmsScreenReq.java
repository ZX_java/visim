package com.traffic.visim.vms.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName: VmsScreenReq
 * @Description: TODO 类注释/描述
 * @module: jeecg-boot-parent
 * @Author: zhouzheng
 * @date: 2023/11/28
 * Copyright© Shandong New Beiyang Information Technology Co., Ltd. . all rights reserved.
 */
@Data
public class VmsScreenReq implements Serializable {
//屏宽。像素。
    private int nWidthScreen = 626;
//屏高。像素。
    private int nHeightScreen = 512;
//屏幕颜色。 0单基色，1双基色，2三基色。
    private int nColor = 0;
//灰度级别。1、2、3、4、5、6、7、8 （默认为1）
    private int nGray = 1;
//控制卡类型 ，默认值为0。
    private int nCardType = 0;

    private int pExParamsBuf = 0;

    private int nBufSize = 0;
}
