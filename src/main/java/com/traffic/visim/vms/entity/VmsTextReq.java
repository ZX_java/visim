package com.traffic.visim.vms.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName: VmsTextReq
 * @Description: TODO 类注释/描述
 * @module: jeecg-boot-parent
 * @Author: zhouzheng
 * @date: 2023/11/28
 * Copyright© Shandong New Beiyang Information Technology Co., Ltd. . all rights reserved.
 */
@Data
public class VmsTextReq implements Serializable {
    //指定区域ID。
    private int nAreaID;
    //文字。utf-16字符串。
    private String pText;
    //文字颜色。RGB
    private int nTextColorR =255;
    private int nTextColorG =0;
    private int nTextColorB =0;
    //背景颜色。RGB
    private int nBackGroupColor;
    //文字风格。对齐方式：0x0000左上对齐 0x0001居中上对齐 0x0002右上对齐 0x0003居中左对齐 0x0004水平垂直居中 0x0005 居中右对齐 0x0006左下对齐 0x0007居中下对齐 0x0008 右下对齐
    // 字体属性：0x0100粗体 0x0200下划线 0x0400斜体 （字体属性可以和对齐方式组合） 例如： 0x0004|0x0100 |0x0200下划线粗体居中
    private int nStyle;
    //字体名字。utf-16字符串。
    private String pFontName="Arial";
    //字体高度。（像素）
    private int nFontHeight =80;
    //显示效果ID。详情
    private int nShowEffect =0;
    //显示速度，毫秒。一般写25。
    private int nShowSpeed=25;
    //清屏效果。201为:不清屏,0为: 立即清除。如果nShowEffect为202、203、204、205则该值应该设置为201
    private int nClearType =0;
    //停留时间,秒。连续移动时应该把此值设置为0。其他情况下一般为3。
    private int nStayTime =0;
    //额外参数缓冲区，设置为0,暂不使用。
    private int pExParamsBuf=0;
    //额外参数缓冲区长度，设置为0,暂不使用。
    private int nBufSize =0;
}
