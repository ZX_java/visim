package com.traffic.visim.vms.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName: VmsProgramReq
 * @Description: TODO 类注释/描述
 * @module: jeecg-boot-parent
 * @Author: zhouzheng
 * @date: 2023/11/28
 * Copyright© Shandong New Beiyang Information Technology Co., Ltd. . all rights reserved.
 */
@Data
public class VmsProgramReq implements Serializable {
//节目边框图片路径。utf-16字符串。不使用边框设置为0。
    private int pBoderImgPathProgram =0;
//边框特效。0顺时针 1逆时针 2闪烁 3静止
    private int nBorderEffectProgram =3;
//边框速度。  1~9,默认为5.
    private int nBorderSpeedProgram = 5;
//额外参数缓冲区，设置为0,暂不使用。
    private int pExParamsBuf = 0;
//额外参数缓冲区长度，设置为0,暂不使用。
    private int nBufSize =0 ;
}
