package com.traffic.visim.vms.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName: VmsAreaReq
 * @Description: TODO 类注释/描述
 * @module: jeecg-boot-parent
 * @Author: zhouzheng
 * @date: 2023/11/28
 * Copyright© Shandong New Beiyang Information Technology Co., Ltd. . all rights reserved.
 */
@Data
public class VmsAreaReq implements Serializable {
    //指定的节目ID。
   private int nProgramID;
   //区域X起点。
    private int nX =84;
    //区域Y起点。
    private int nY =94;
    //区域宽度
    private int nWidthArea =433;
    //区域高度
    private int nHeightArea =296;

    //区域边框图片路径。utf-16字符串。不使用边框设置为0。
    private int pBoderImgPath =0;
    //边框特效。0顺时针 1逆时针 2闪烁 3静止
    private int nBorderEffect =3;
    //边框速度。  1~9。
    private int nBorderSpeed =5;
    private int pExParamsBuf =0;
    private int nBufSize =0;
}
