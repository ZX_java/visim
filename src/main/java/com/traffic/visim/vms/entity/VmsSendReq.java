package com.traffic.visim.vms.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName: VmsSendReq
 * @Description: TODO 类注释/描述
 * @module: jeecg-boot-parent
 * @Author: zhouzheng
 * @date: 2023/11/28
 * Copyright© Shandong New Beiyang Information Technology Co., Ltd. . all rights reserved.
 */
@Data
public class VmsSendReq implements Serializable {
//发送类型。0 TCP 1串口 2 UDP
    private int nSendType;
//发送参数。UTF-16字符串。当发送类型为0或者2时 pStrParams填写IP地址例如"192.168.2.200"，默认连接6101端口，如果使用了端口映射，pStrParams应该加上端口信息，例如"192.168.2.200:6000"，6000为端口号  。 当发送类型为1时, pStrParams填写串口信息，例如"4:115200" , 4为串口号，115200为串口波特率。 如果不清楚设备波特率，可以使用Cmd_GetBaudRate函数获得设备波特率。
    private String pStrParams;
//设备ID。UTF-16字符串.一般为0,使用485通讯时必须填写设备ID。
    private int pDeviceGUID;

    private int pExParamsBuf;

    private int nBufSize;
}
