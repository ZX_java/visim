package com.traffic.visim.vms.controller;

import com.traffic.visim.common.Result;

import com.traffic.visim.vms.entity.*;
import com.traffic.visim.vms.service.IVmsService;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName: VmsController
 * @Description: TODO 类注释/描述
 * @module: jeecg-boot-parent
 * @Author: zhouzheng
 * @date: 2023/11/28
 * Copyright© Shandong New Beiyang Information Technology Co., Ltd. . all rights reserved.
 */

@RestController
@RequestMapping("/vms/vmsCmd")
@Slf4j
public class VmsController {
        @Autowired
        private IVmsService vmsService;


        @GetMapping(value = "/send")
        public Result<Integer> send(VmsSendReq vmsSendReq, VmsAreaReq vmsAreaReq, VmsProgramReq vmsProgramReq, VmsScreenReq vmsScreenReq, VmsTextReq vmsTextReq,
                                    HttpServletRequest req) {
                int result = vmsService.send(vmsSendReq, vmsAreaReq, vmsProgramReq, vmsScreenReq, vmsTextReq);
            return Result.OK(result);
        }

    @GetMapping(value = "/clear")
    public Result<Integer> clear(VmsClearScreenReq vsClearScreenReq, HttpServletRequest req) {
        int result = vmsService.clear(vsClearScreenReq);
        return Result.OK(result);
    }

    @GetMapping(value = "/ping")
    public Result<Integer> ping(VmsCardOnlineReq vmsCardOnlineReq, HttpServletRequest req) {
        int result = vmsService.ping(vmsCardOnlineReq);
        return Result.OK(result);
    }

    @GetMapping(value = "/luminance")
    public Result<Integer> luminance(VmsSetLuminanceReq vmsSetLuminanceReq, HttpServletRequest req) {
        int result = vmsService.luminance(vmsSetLuminanceReq);
        return Result.OK(result);
    }
}
