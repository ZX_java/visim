package com.traffic.visim.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/***
 * 获取交通服务信息
 */
@Configuration
@Data
public class TrafficConfig {
    //服务ip
    @Value("${traffic.server.ip:127.0.0.1}")
    private String url;
    //服务端口
    @Value("${traffic.server.port}")
    private String port;
    //服务项目名称
    @Value("${traffic.server.project}")
    private String projectName;

    //默认登录用户
    @Value("${traffic.user.username}")
    private String userName;
    //默认登录用户密码
    @Value("${traffic.user.password}")
    private String password;

    //获取token方法
    @Value("${traffic.api.login}")
    private String loginUrl;


    //写入路口实例
    @Value("${traffic.api.addCross}")
    private String addCross;

    @Value("${$traffic.token.header:X-Access-Token}")
    private String tokenKey;

}
