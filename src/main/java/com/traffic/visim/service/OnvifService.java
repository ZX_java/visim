package com.traffic.visim.service;

import com.alibaba.fastjson.JSONObject;
import com.traffic.visim.common.Result;
import com.traffic.visim.onvif.dto.ImageInfo;
import com.traffic.visim.onvif.dto.PtzRequest;

import javax.print.DocFlavor;

public interface OnvifService {
   public void movePtz(PtzRequest request);

   public PtzRequest getPtz(String ip);

   public void sendEvent(String ip, Integer status);

   public void saveOnvif(String ip);

   public void saveAllOnvif();

   public void createImage(ImageInfo info);
}
