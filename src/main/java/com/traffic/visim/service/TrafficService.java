package com.traffic.visim.service;

import com.alibaba.fastjson.JSONObject;
import com.traffic.visim.common.Result;

public interface TrafficService {
    //获取token
    public String getToken() throws Exception;

    public void saveTokenToRedis() throws Exception;

    //写入路口数据
    public Result AddCross(JSONObject object) throws Exception;
}
