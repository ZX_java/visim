package com.traffic.visim;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VisimApplication {

    public static void main(String[] args) {
        SpringApplication.run(VisimApplication.class, args);
    }

}
