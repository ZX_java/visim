package com.traffic.visim.log.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.traffic.visim.log.entity.PermissionIp;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface PermissionIpMapper extends BaseMapper<PermissionIp> {
    @Select("select s.ip from tra_monitor s where s.is_vm=1 and s.type=1 GROUP BY s.ip")
    List<String> getIpList();
}
