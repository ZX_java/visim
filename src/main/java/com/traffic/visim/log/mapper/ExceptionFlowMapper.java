package com.traffic.visim.log.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.traffic.visim.log.entity.ExceptionFlow;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ExceptionFlowMapper extends BaseMapper<ExceptionFlow> {
}
