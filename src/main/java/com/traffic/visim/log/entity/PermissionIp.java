package com.traffic.visim.log.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("permission_ip")
public class PermissionIp {
    @TableId(value = "id",type = IdType.AUTO)
    private java.lang.Integer id;

    private String ip;
    private Integer state;
}
