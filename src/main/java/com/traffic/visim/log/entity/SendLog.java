package com.traffic.visim.log.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SendLog {
    //0 攔截器 ，1越權
    private Integer type;
    private String ip;
    //0 代理，1控制中心 ，2.模拟系统 3.车联网
    private Integer systemType;
}
