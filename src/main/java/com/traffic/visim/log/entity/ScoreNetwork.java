package com.traffic.visim.log.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName("score_network")
public class ScoreNetwork {
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    private Date startTime;
    private Date endTime;
    private double score;
}
