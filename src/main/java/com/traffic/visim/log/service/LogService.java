package com.traffic.visim.log.service;

import com.traffic.visim.log.entity.ExceptionFlow;
import com.traffic.visim.log.entity.ExceptionIp;
import com.traffic.visim.log.entity.ExceptionPermission;

public interface LogService {
    public void saveExceptionIp(ExceptionIp exceptionIp);
    public void saveExceptionFlow(ExceptionFlow exceptionFlow);
    public void saveExceptionPermission(ExceptionPermission exceptionPermission);
    public boolean findIp(String ip);
    public long selectCountIp();
    public long selectCountFlow();
    public long selectCountPermission();
}
