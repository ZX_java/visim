package com.traffic.visim.log.service.impl;

import com.traffic.visim.log.entity.ScoreNetwork;
import com.traffic.visim.log.mapper.ScoreNetworkMapper;
import com.traffic.visim.log.service.LogService;
import com.traffic.visim.log.service.ParameterService;
import com.traffic.visim.log.service.ScoreService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
@Slf4j
public class ScoreServiceImpl implements ScoreService {
    @Autowired
    ParameterService parameterService;
    @Autowired
    LogService logService;
    @Autowired
    ScoreNetworkMapper scoreNetworkMapper;
    @Override
    public double setiPScore() {
        long ipCount=logService.selectCountIp();
        int ipScore = 0;
        if (ipCount<=50){
            String key = parameterService.selectException("exception_ip_0~50");
            ipScore = 100-Integer.parseInt(key);
        } else if (51<ipCount && ipCount<=200) {
            String key = parameterService.selectException("exception_ip_51~200");
            ipScore = 100-Integer.parseInt(key);
        } else if (201<ipCount && ipCount<=500) {
            String key = parameterService.selectException("exception_ip_201~500");
            ipScore = 100-Integer.parseInt(key);
        } else if (501<ipCount && ipCount<1000) {
            String key = parameterService.selectException("exception_ip_501~1000");
            ipScore = 100-Integer.parseInt(key);
        }else {
            String key = parameterService.selectException("exception_ip>1000");
            ipScore = 100-Integer.parseInt(key);
        }
        String key = parameterService.selectException("exception_ip_weight");
        double ipWeight = Double.parseDouble(key);
        return ipScore*ipWeight*0.01;
    }

    @Override
    public double setFlowScore() {
        long flowCount=logService.selectCountFlow();
        int flowScore = 0;
        if (flowCount<=50){
            String key = parameterService.selectException("exception_ip_0~50");
            flowScore = 100-Integer.parseInt(key);
        } else if (51<flowCount && flowCount<=200) {
            String key = parameterService.selectException("exception_ip_51~200");
            flowScore = 100-Integer.parseInt(key);
        } else if (201<flowCount && flowCount<=500) {
            String key = parameterService.selectException("exception_ip_201~500");
            flowScore = 100-Integer.parseInt(key);
        } else if (501<flowCount && flowCount<1000) {
            String key = parameterService.selectException("exception_ip_501~1000");
            flowScore = 100-Integer.parseInt(key);
        }else {
            String key = parameterService.selectException("exception_ip>1000");
            flowScore = 100-Integer.parseInt(key);
        }
        String key = parameterService.selectException("exception_flow_weight");
        double flowWeight = Double.parseDouble(key);
        return flowScore*flowWeight*0.01;
    }

    @Override
    public double setPermissionScore() {
        long permissionCount=logService.selectCountPermission();
        int permissionScore = 0;
        if (permissionCount<=50){
            String key = parameterService.selectException("exception_ip_0~50");
            permissionScore = 100-Integer.parseInt(key);
        } else if (51<permissionCount && permissionCount<=200) {
            String key = parameterService.selectException("exception_ip_51~200");
            permissionScore = 100-Integer.parseInt(key);
        } else if (201<permissionCount && permissionCount<=500) {
            String key = parameterService.selectException("exception_ip_201~500");
            permissionScore = 100-Integer.parseInt(key);
        } else if (501<permissionCount && permissionCount<1000) {
            String key = parameterService.selectException("exception_ip_501~1000");
            permissionScore = 100-Integer.parseInt(key);
        }else {
            String key = parameterService.selectException("exception_ip>1000");
            permissionScore = 100-Integer.parseInt(key);
        }
        String key = parameterService.selectException("exception_permission_weight");
        double permissionWeight = Double.parseDouble(key);
        return permissionScore*permissionWeight*0.01;
    }

    @Override
    public void setScore() {
        double score = setFlowScore()+setiPScore()+setPermissionScore();
        Date fiveTime = new Date(System.currentTimeMillis() - 5*60*1000);
        Date nowTime = new Date();
        ScoreNetwork scoreNetwork = ScoreNetwork.builder().startTime(fiveTime).endTime(nowTime).score(score).build();
        scoreNetworkMapper.insert(scoreNetwork);
    }

    @Override
    @Transactional
    public void doXxlJob() {
        log.info("进入定时采集任务");
        setScore();
    }

}
