package com.traffic.visim.log.service;

public interface ScoreService {
    public double setiPScore();
    public double setFlowScore();
    public double setPermissionScore();
    public void setScore();
    public void doXxlJob();
}
