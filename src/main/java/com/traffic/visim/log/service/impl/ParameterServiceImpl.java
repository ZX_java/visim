package com.traffic.visim.log.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.traffic.visim.log.entity.SysParameter;
import com.traffic.visim.log.mapper.SysParameterMapper;
import com.traffic.visim.log.service.ParameterService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ParameterServiceImpl implements ParameterService {
    @Autowired
    SysParameterMapper sysParameterMapper;
    @Override
    public String selectException(String key) {
        LambdaQueryWrapper<SysParameter> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysParameter::getParamKey,key);
        SysParameter sysParameter=sysParameterMapper.selectOne(queryWrapper);
        return sysParameter.getParmValue();
    }
}
