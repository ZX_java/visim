package com.traffic.visim.onvif.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("tra_onvif_event")
public class OnvifEvent {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    //ip地址
    private String ip;
    //类型，3为违章停车，4为闯红灯
    private Integer type;
    //创建时间
    private Date createTime;
    //图片地址
    private String url;

    @TableField(exist = false)
    private String typeStr;
    @TableField(exist = false)
    private String fullUrl;

}
