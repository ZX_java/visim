package com.traffic.visim.onvif;

import de.onvif.soap.OnvifDevice;
import de.onvif.soap.devices.MediaDevices;
import org.onvif.ver10.device.wsdl.GetDeviceInformationResponse;
import org.onvif.ver10.schema.Profile;


public class ClientDemo {

    public static void main(String[] args) {
        try {
            String cameraIP = "172.16.30.17"; // 摄像头的IP地址
            String username = "test"; // 访问摄像头的用户名
            String password = "test"; // 访问摄像头的密码

            // 创建ONVIF设备对象
            OnvifDevice onvifDevice = new OnvifDevice(cameraIP,username,password);
            // 获取设备信息
            GetDeviceInformationResponse deviceInfoResponse = onvifDevice.getDevices().getDeviceInformation();

            String manufacturer = deviceInfoResponse.getManufacturer();
            String model = deviceInfoResponse.getModel();
            String firmwareVersion = deviceInfoResponse.getFirmwareVersion();
            String serialNumber = deviceInfoResponse.getSerialNumber();

            System.out.println("Manufacturer: " + manufacturer);
            System.out.println("Model: " + model);
            System.out.println("Firmware Version: " + firmwareVersion);
            System.out.println("Serial Number: " + serialNumber);

            Profile profile=onvifDevice.getDevices().getProfiles().get(0);

            MediaDevices media = onvifDevice.getMedia();
            System.out.println(media.getRTSPStreamUri(profile.getToken()));

//            // 获取设备支持的服务列表
//            GetServicesResponse servicesResponse = onvifDevice.
//            List<Service> services = servicesResponse.getService();
//
//            System.out.println("Supported Services:");
//            for (Service service : services) {
//                System.out.println(service.getServiceType());
//            }
//
//            // 断开连接
//            onvifDevice.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
