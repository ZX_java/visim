package com.traffic.visim.onvif.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.traffic.visim.onvif.dto.SignInfo;
import com.traffic.visim.onvif.entity.OnvifEvent;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
public interface IOnvifEventMapper extends BaseMapper<OnvifEvent> {
    @Select("select s.ip,c.roadnet_id,c.cross_name,p.number,c.vissim_code,s.monitor_info from tra_monitor s left join tra_cross c on c.id=s.location_id left join tra_roadnet e on e.id=c.roadnet_id left join tra_signal p on p.cross_id=s.location_id  where s.ip=#{ip}  and e.effice_status=0;")
    List<SignInfo> getSignInfoByMonitorIP(String ip);
}
