package com.traffic.visim.onvif.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.traffic.visim.onvif.entity.OnvifEvent;
import com.traffic.visim.util.PagerModel;

public interface OnvifEventService {
    public void saveOnvif(OnvifEvent onvifEvent);

    public PagerModel<OnvifEvent> getPageList(Page<OnvifEvent> event, QueryWrapper<OnvifEvent> queryWrapper);

}
