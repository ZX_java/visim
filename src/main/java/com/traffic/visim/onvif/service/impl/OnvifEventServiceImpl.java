package com.traffic.visim.onvif.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.traffic.visim.onvif.entity.OnvifEvent;
import com.traffic.visim.onvif.mapper.IOnvifEventMapper;
import com.traffic.visim.onvif.service.OnvifEventService;
import com.traffic.visim.util.PagerModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class OnvifEventServiceImpl implements OnvifEventService {
    @Autowired
    IOnvifEventMapper onvifEventMapper;

    @Value("${nginx.ip}")
    private String nginxIp;
    @Value("${nginx.port}")
    private String nginxPort;

    @Override
    public void saveOnvif(OnvifEvent onvifEvent) {
        onvifEventMapper.insert(onvifEvent);
    }

    @Override
    public PagerModel<OnvifEvent> getPageList(Page<OnvifEvent> page, QueryWrapper<OnvifEvent> queryWrapper) {
        Page<OnvifEvent> userIPage = onvifEventMapper.selectPage(page, queryWrapper);
        // 分页的所有数据都在userPage对象中封装着
        // 获取总页数
        long pages = userIPage.getPages();
        //一页显示几条数据
        long size = userIPage.getSize();
        // 获取当前页
        long current = userIPage.getCurrent();
        // 获取当前页数据集合
        List<OnvifEvent> records = userIPage.getRecords();
        if (CollUtil.isNotEmpty(records)) {
            records.forEach(re -> {
                        re.setFullUrl("http://" + nginxIp + ":" + nginxPort + "/" + re.getUrl());
                        re.setTypeStr(3 == re.getType() ? "违章停车" : "闯红灯");
                    }
            );
        }
        // 获取总记录数
        long total = userIPage.getTotal();
        // 当前页是否有下一页
        boolean hasNext = userIPage.hasNext();
        // 当前页是否有上一页
        boolean hasPrevious = userIPage.hasPrevious();

        return new PagerModel<>(userIPage.getTotal(), records);
    }
}
