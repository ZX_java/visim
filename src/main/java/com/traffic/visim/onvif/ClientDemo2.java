package com.traffic.visim.onvif;

import de.onvif.soap.OnvifDevice;
import de.onvif.soap.devices.MediaDevices;
import de.onvif.soap.devices.PtzDevices;
import org.onvif.ver10.device.wsdl.GetDeviceInformationResponse;
import org.onvif.ver10.schema.Profile;

import java.net.ConnectException;
import java.util.List;


public class ClientDemo2 {

    public static void main(String[] args) {
        try {
            String cameraIP = "172.16.30.24"; // 摄像头的IP地址
            String username = "test"; // 访问摄像头的用户名
            String password = "test"; // 访问摄像头的密码

            // 创建ONVIF设备对象
            OnvifDevice onvifDevice = new OnvifDevice(cameraIP,username,password);
            List<Profile> profiles = onvifDevice.getDevices().getProfiles();
            Profile profile1=profiles.get(0);
            String token= profile1.getToken();
            PtzDevices ptz = onvifDevice.getPtz();
            ptz.absoluteMove(token,0.9f,0.9f,0f);
        } catch (ConnectException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
