package com.traffic.visim.onvif.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PtzRequest {
    //信号机id
    private String ip;
//    //x轴变化(从0~1)
//    private float x;
//    //y轴变化(从0~1)
//    private float y;
//    //焦距变化(从0~1)
//    private float z;
    // 0左+，1右+，2上+，3下+，4焦距+，5焦距-
    private Integer type;
}
