package com.traffic.visim.onvif.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OnvifDto {
    private String manufacture;
    private String model;
    private String firmwareVersion;

    private String serialNumber;

    private String hardwareId;

    private String ip;

    private String rtspUrl;
}
