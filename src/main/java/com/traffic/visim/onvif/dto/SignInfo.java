package com.traffic.visim.onvif.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SignInfo {
    private String ip;
    private Integer corssId;
    private String monitorInfo;
    private Integer number;
    private Integer roadnetId;
    private String crossName;
}
