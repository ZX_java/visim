package com.traffic.visim.controller;

import com.alibaba.fastjson.JSONObject;
import com.traffic.visim.common.Result;
import com.traffic.visim.service.TrafficService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/traffic")
public class TrafficController {
    @Autowired
    private TrafficService trafficService;

    @PostMapping(value = "/getToken")
    @ResponseBody
    public Result getToken(@RequestBody Map map) throws Exception {
        trafficService.saveTokenToRedis();
        return Result.OK();
    }


    @PostMapping(value = "/addCross")
    @ResponseBody
    public Result addCross(@RequestBody Map map) throws Exception {
        JSONObject object=new JSONObject(map);
        return trafficService.AddCross(object);
    }
}
