package com.traffic.visim.controller;

import com.traffic.visim.common.Result;
import com.traffic.visim.log.service.LogService;
import com.traffic.visim.log.service.ParameterService;
import com.traffic.visim.log.service.ScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/score")
public class ScoreController {
    @Autowired
    ScoreService scoreService;
    @PostMapping("/setScore")
    public Result<String> setScore(){
        scoreService.setScore();
        return Result.OK("打分成功");
    }
}
