package com.traffic.visim.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.traffic.visim.common.Result;
import com.traffic.visim.onvif.dto.PtzRequest;
import com.traffic.visim.onvif.entity.OnvifEvent;
import com.traffic.visim.onvif.service.OnvifEventService;
import com.traffic.visim.service.OnvifService;
import com.traffic.visim.util.PagerModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
@CrossOrigin
@RestController
@RequestMapping("/onvif")
public class OnvifController {
    @Autowired
    OnvifService onvifService;

    @Autowired
    OnvifEventService onvifEventService;

    @GetMapping(value = "/info")
    public @ResponseBody void date(@RequestBody Map map) {
        List<String> ips = (List<String>) map.get("ips");
        if (CollUtil.isNotEmpty(ips)) {
            ips.forEach(ip -> {
                onvifService.saveOnvif(ip);
            });
        }
    }

    @GetMapping(value = "/infoAll")
    public @ResponseBody void infoAll(@RequestBody Map map) {
       onvifService.saveAllOnvif();
    }

    /****
     * 获取设备当前横纵信息
     * @param map
     * @return
     */
    @PostMapping(value = "/getPtzLocation")
    public @ResponseBody Result getPtzLocation(@RequestBody Map map)
    {
        String ip= (String) map.get("ip");
        if (ObjectUtil.isEmpty(ip))
        {
            throw new RuntimeException("ip为空");
        }
        return Result.OK(onvifService.getPtz(ip));
    }

    /***
     * 移动设备信息
     * @param request
     * @return
     */
    @PostMapping(value = "/movePtzLocation")
    public Result movePtzLocation(@RequestBody PtzRequest request)
    {
        if (ObjectUtil.isEmpty(request.getIp()))
        {
            throw new RuntimeException("ip为空");
        }
//        if (ObjectUtil.isEmpty(request.getX()))
//        {
//            throw new RuntimeException("x为空");
//        }
        onvifService.movePtz(request);
        return  Result.ok();
    }
    @PostMapping("/eventPageList")
    public Result<PagerModel<OnvifEvent>> getPageEventList(@RequestParam(value = "ip", defaultValue = "") String ip,
                                                           @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                           @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize)
    {
        if (ObjectUtil.isEmpty(ip))
        {
            return Result.error(404,"ip不能为空");
        }
        // 条件构造器
        QueryWrapper<OnvifEvent> queryWrapper = new QueryWrapper<>();
        // 模糊查询Like
        queryWrapper.eq("ip", ip).orderByDesc("url");
        // 分页插件
        Page<OnvifEvent> page = new Page<>(pageNum, pageSize);
        // 查询数据
        return Result.OK(onvifEventService.getPageList(page,queryWrapper));
    }
}
