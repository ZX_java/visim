package com.traffic.visim.job;

import com.traffic.visim.log.service.ScoreService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class NetworkSecure {
    @Autowired
    ScoreService scoreService;
    @XxlJob("networkSecure")
    public ReturnT<String> capture(){
        scoreService.doXxlJob();
        return ReturnT.SUCCESS;
    }
}
